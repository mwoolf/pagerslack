# frozen_string_literal: true

namespace :deploy do
  task :upload_config do
    on roles(:all) do |_host|
      within shared_path.to_s do
        upload! 'oncall.csv', 'oncall.csv'
        upload! 'holidays.yml', 'holidays.yml'
      end
    end
  end

  after :set_current_revision, :upload_config
end
