# frozen_string_literal: true

require 'active_support/configurable'
require 'active_support/core_ext/numeric/time'

class Config
  include ActiveSupport::Configurable

  config_accessor(:command) { '/devoncall' }
  config_accessor(:channel) { '#dev-escalation' }
  config_accessor(:slack_output) { true }
  config_accessor(:positive_reaction) { 'eyes' }
  config_accessor(:members_expire_time) { 15.days.to_i }
  config_accessor(:next_ping_wait) { 2.minutes.to_i }
  config_accessor(:max_pings) { 6 }
  config_accessor(:max_member_count) { 600 }
  config_accessor(:unavailable_emojis) { %w[:palm_tree: :hospital: :thermometer: :face_with_thermometer: :away: :sun_with_face: :fork_and_knife: :ferris_wheel: :waxing_gibbous_moon: :baby: :no_entry: :beach_with_umbrella: :umbrella_on_ground:] }
end
