# frozen_string_literal: true

require 'cgi'
require 'semantic_logger'
require 'sinatra/base'
require 'sinatra/custom_logger'
require 'slack-ruby-client'
require './lib/chat/message_parser'
require './lib/chat/reaction_responder'

# This class contains all of the webserver logic for processing incoming requests from Slack.
class API < Sinatra::Base
  helpers Sinatra::CustomLogger

  # TODO: Refactor this so more than 1 active incident can be reported.
  EVENT = Concurrent::Event.new

  configure :development, :production do
    SemanticLogger.default_level = :trace

    # Log to a file, and use the colorized formatter
    SemanticLogger.add_appender(file_name: './log/pagerslack.log', formatter: :color)
    SemanticLogger.add_appender(file_name: './log/pagerslack.json.log', formatter: :json)

    logger = SemanticLogger['pagerslack']

    set :logger, logger
    use Rack::CommonLogger, logger
  end

  post '/incident' do
    Thread.new { Chat::MessageParser.new(EVENT, params).parse }
  end

  # This is the endpoint Slack will post Event data to.
  post '/events' do
    # Extract the Event payload from the request and parse the JSON
    request_data = JSON.parse(request.body.read)
    # Check the verification token provided with the request to make sure it matches the verification token in
    # your app's setting to confirm that the request came from Slack.
    unless SLACK_CONFIG[:slack_verification_token] == request_data['token']
      halt 403, "Invalid Slack verification token received: #{request_data['token']}"
    end

    case request_data['type']
      # When you enter your Events webhook URL into your app's Event Subscription settings, Slack verifies the
      # URL's authenticity by sending a challenge token to your endpoint, expecting your app to echo it back.
      # More info: https://api.slack.com/events/url_verification
    when 'url_verification'
      request_data['challenge']
    when 'event_callback'
      # Get the Team ID and Event data from the request object
      # team_id = request_data['team_id']
      event_data = request_data['event']

      # Events have a "type" attribute included in their payload, allowing you to handle different
      # Event payloads as needed.
      if event_data['type'] == 'message' && event_data['channel_type'] == 'im' && !event_data['bot_id']
        Thread.new { Chat::MessageParser.new(EVENT, event_data).parse }
      elsif event_data['type'] == 'reaction_added'
        Thread.new { Chat::ReactionResponder.new(EVENT, event_data).respond }
      elsif event_data['type'] == 'member_left_channel'
        Thread.new { Chat::Members.remove_member(event_data['user']) }
      end

      # Return HTTP status code 200 so Slack knows we've received the Event
      status 200
    end
  end
end
