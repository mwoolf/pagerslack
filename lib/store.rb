# frozen_string_literal: true

require 'redis'

class Store
  NAMESPACE = 'pagerslack'
  MIN_RANK = 0
  MAX_RANK = 9_999_999_999

  class << self
    def zadd(key, *options)
      client.zadd(prefixed(key), *options)
    end

    def zrem(key, value)
      client.zrem(prefixed(key), value)
    end

    def zrangebyscore(key, *options)
      client.zrangebyscore(prefixed(key), *options)
    end

    def zrevrangebyscore(key, *options)
      client.zrevrangebyscore(prefixed(key), *options)
    end

    def set(key, value, options = {})
      client.set(prefixed(key), value, options)
    end

    def del(key)
      client.del(prefixed(key))
    end

    def get(key)
      client.get(prefixed(key))
    end

    def zrange(key)
      client.zrange(prefixed(key), MIN_RANK, MAX_RANK)
    end

    private

    def prefixed(key)
      "#{NAMESPACE}:#{key}"
    end

    def client
      Redis.current
    end
  end
end
