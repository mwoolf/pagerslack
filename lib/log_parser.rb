# frozen_string_literal: true

require 'yajl'

class LogParser
  def initialize(file:)
    @json = File.new(file, 'r')
    @incidents = {}
  end

  def parse
    parser = Yajl::Parser.new

    parser.parse(@json, buffer_size = 10_000) do |obj|
      incident_url = obj['payload']&.dig('issue_url')

      next unless incident_url
      next if incident_url.include?('test')

      @incidents[incident_url] ||= default_incident_hash
      @incidents[incident_url].merge!(incident_hash(incident_url, obj))
    end

    @incidents.values
  end

  private

  def incident_hash(incident_url, obj)
    payload = obj['payload']

    {}.tap do |incident|
      incident[:incident_url] = incident_url
      @incidents[incident_url][:attempts] += 1 if obj['message']&.include?('User pinged')

      case obj['message']
      when 'New incident'
        incident[:reported_by] = payload['reported_by']
        incident[:reported_at] = obj['timestamp']
      when 'Time to response'
        incident[:time_to_response] = obj['duration_ms']
        incident[:time_at_response] = obj['timestamp']
      when 'No weekday engineers found, trying with weekend engineers'
        incident[:weekend_pinged] = true
      when 'user unavailable'
        incident[:unavailable] = true
      end
    end
  end

  def default_incident_hash
    {
      attempts: 0,
      weekend_pinged: false,
      unavailable: false
    }
  end
end
