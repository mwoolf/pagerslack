# frozen_string_literal: true

require './lib/config'

module Chat
  class ReactionResponder
    include SemanticLogger::Loggable

    def initialize(event, event_data)
      @event = event
      @user_id = event_data['user']
      @ts = event_data['item']&.dig('ts') || event_data['ts']
      @reaction = event_data['reaction']
    end

    def respond
      return if Config.slack_output && @reaction != Config.positive_reaction
      return if Config.slack_output && Store.get(:active_ping) != @ts

      Store.del(:active_ping)

      return if @event.set?

      @event.set

      Store.zadd(:members, [Time.now.to_i, @user_id], { nx: false, xx: true })
      logger.info('User reacted', username: username)

      client.message("#{username} thank you for looking into this! :heart:", ts: @ts)
    end

    private

    def client
      Chat::Client.instance
    end

    # TODO: No need to call the API, we should retrieve the name from the CSV
    def username
      @username ||= client.get_username(@user_id)
    end
  end
end
