ounsignedcommitfsdkjlfhsgdjffddssdjkf# PagerSlack

PagerSlack is a bot server for Slack that handles an oncall rotation based on the online presence of a globally distributed team.

## How does it work?

Invite the bot to your channel and leave a message there with `/devoncall http://incident.url` to trigger an incident. The bot will ping an online engineer within the channel in a fair way (less pinged first). 

The engineer can then react with a :eyes: to ACK. If nobody ACKs after X minutes, the bot will try to find the next online engineer and so on.

Other engineers available can react too - in all cases, the person who reacts will end at the bottom of the queue for newer incidents.

### Other helpful commands

Send a private message to the bot with:

`top` will give you the top 25 members that are next in the queue, and therefore may get pinged in the next incidents relatively soon.

`position` will tell you your position in the queue. A higher number means less probabilities for you to get pinged when an incident is triggered.

## Reporting

While we use Redis for caching and storage, logs (in JSON) could be parsed to know about metrics like:

- Average time to response
- Incidents per period
- TZs more affected

### Installation

PagerSlack requires Redis and Ruby to run.

Assign your app's tokens and global variables:

```
export SLACK_CLIENT_ID="XXXX.XXXX"
export SLACK_API_SECRET="XXXX"
export SLACK_VERIFICATION_TOKEN="XXXX"
export SLACK_REDIRECT_URI="XXXX"
```

Install the gems with `bundler` and run the app with `rackup`

```sh
$ cd pagerslack
$ bundle
$ rackup
```

### License
----
MIT
