# frozen_string_literal: true

require 'spec_helper'
require 'semantic_logger'
require './lib/chat/incident_summary'
require 'slack'

describe Chat::IncidentSummary do
  let(:ts) { Time.new(2020).to_f }
  let(:ack_member) { 'member2' }
  subject(:summary) { described_class.new(reporter: 'reporter', pinged_members: %w[member1 member2], ack_member: ack_member, ts: ts) }

  describe '#send' do
    context 'acknowledged' do
      it 'sends the right message' do
        expect(Chat::Client.instance).to receive(:message).with("*Incident Summary*\n\nReported by: <@reporter>\n\nTime to response: <4 minutes.", ts: ts)

        summary.post
      end
    end

    context 'not acknowledged' do
      let(:ack_member) { nil }

      it 'sends the right message' do
        expect(Chat::Client.instance).to receive(:message).with("*Incident Summary*\n\nReported by: <@reporter>\n\nTime without response: <4 minutes.", ts: ts)

        summary.post
      end
    end
  end
end
